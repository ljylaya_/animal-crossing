//
//  UsersViewModel.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/24/20.
//

import Foundation
import ReactiveSwift
import RealmSwift

class UsersViewModel {
    var realm: Realm!
    var loginValue: LoginValue!
    var signUpValue: SignUpValue!
    var loginObserver = MutableProperty(())
    var signUpObserver = MutableProperty(())
    var loginSignUpErrMessage = MutableProperty("")
    var isSignUp = false
    
    init(forSignUp: Bool) {
        isSignUp = forSignUp
        render()
    }
    
    func render() {
        do {
            realm = try Realm()
            let users = realm.objects(User.self)
            if isSignUp {
                doSignUpAction(users: users)
            } else {
                doLoginAction(users: users)
            }
        } catch {
            fatalError("setting up view model")
        }
        
    }
    
    func doSignUpAction(users: Results<User>) {
        signUpObserver.signal.observeValues { [weak self] (_) in
            guard let strongSelf = self else {
                return
            }
            if strongSelf.isUserExist(userObject: users) {
                strongSelf.loginSignUpErrMessage.value = "User email already exist!"
            } else {
                let newUser = User()
                newUser.email = strongSelf.signUpValue.email
                newUser.username = strongSelf.signUpValue.username
                newUser.password = strongSelf.signUpValue.password
                UserDefaults.standard.setLoggedIn(value: true)
                UserDefaults.standard.setUsername(value: strongSelf.signUpValue.username)
                strongSelf.saveNewUser(userObject: newUser)
                strongSelf.loginSignUpErrMessage.value = ""
            }
            
        }
    }
    
    func doLoginAction(users: Results<User>) {
        loginObserver.signal.observeValues { [weak self] (_) in
            guard let strongSelf = self else {
                return
            }
            if !strongSelf.loginWithUsernamePassword(userObject: users) {
                strongSelf.loginSignUpErrMessage.value = "Username and password did not match!"
            } else {
                strongSelf.loginSignUpErrMessage.value = ""
            }
        }
        
    }
    
    func loginWithUsernamePassword(userObject: Results<User>) -> Bool {
        let userResult = userObject.first { (user) -> Bool in
            if (user.username == loginValue.usernameOrEmail || user.email == loginValue.usernameOrEmail) && user.password == loginValue.password {
                return false
            }
            return true
        }
        return userResult != nil
    }
    
    func doLogin(value: LoginValue) {
        loginValue = value
        loginObserver.value = ()
    }
    
    func doSignUp(value: SignUpValue) {
        signUpValue = value
        signUpObserver.value = ()
    }
    
    func isUserExist(userObject: Results<User>) -> Bool {
        let userResult = userObject.first { (user) -> Bool in
            if (user.email == signUpValue.email) {
                return true
            }
            return false
        }
        return userResult != nil
    }
    
    private func saveNewUser(userObject: User) {
        do {
            try realm.write {
                realm.add(userObject)
            }
        } catch {
            fatalError("saving of user error")
        }
    }
}
