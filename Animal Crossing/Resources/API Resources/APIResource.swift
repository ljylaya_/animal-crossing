//
//  APIResource.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/19/20.
//

import Foundation
import UIKit
/*
 * Implemented https://matteomanferdini.com/network-requests-rest-apis-ios-swift/
 */

protocol APIResource {
    associatedtype ModelType: Decodable
    var methodPath: String { get }
    var queryItems: [URLQueryItem] {get}
}

class VillagerResource: APIResource {
    typealias ModelType = Villager
    let methodPath = "/villagers"
    var queryItems: [URLQueryItem] = []
}

class SingleVillagerResource: APIResource {
    typealias ModelType = Villagers
    let methodPath = "/villagers"
    var queryItems: [URLQueryItem] = []
}

class FossilsResource {
    let methodPath = "/fossils"
    
}

class FishesResource {
    let methodPath = "/fish"
    
}

class BugsResource {
    let methodPath = "/bugs"
    
}

class MusicsResource {
    let methodPath = "/songs"
    
}
