//
//  Enums.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/19/20.
//

import Foundation

// Default app font weight
enum FontWeight: String {
    case bold = "-Bold"
    case regular = ""
    case italic = "-Italic"
    case boldItalic = "-BoldItalic"
}

enum Menu: String {
    case friends = "friends"
    case globe = "globe"
    case critters = "critters"
    case cam = "cam"
    case settings = "settings"
    case map = "map"
    case songs = "songs"
    case update = "update"
    case diy = "diy"
    case flag = "flag"
    case shop = "shop"
    case design = "design"
    case camp = "camp"
    case message = "message"
}

enum ColorHex: String, CaseIterable {
    case hex1 = "BAC7E2"
    case hex2 = "F5D4A6"
    case hex3 = "99CFD7"
    case hex4 = "8365A4"
    case hex5 = "E48581"
    case hex6 = "EDCE6A"
    case hex7 = "54473F"
    case hex8 = "A8D6DE"
    case hex9 = "C2D1AA"
    case hex10 = "F2B15A"
    case hex11 = "CDD3E9"
    case hex12 = "87B578"
    case hex13 = "C3D560"
    case hex14 = "5E6CA9"
    case hex15 = "52453D"
    
}

enum UserDefaultsKeys : String {
    case isLoggedIn
    case username
}

enum DetailType {
    case bug
    case fish
    case fossil
    case seaCreature
    case villager
}
