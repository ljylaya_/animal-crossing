//
//  Constants.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/19/20.
//

import Foundation
import UIKit

class Constants {
    static let shared = Constants()
    var menu: [String] = ["globe", "friends", "critters", "cam", "settings", "map", "songs", "update", "diy", "shop", "design", "camp", "message"
    ]
    let CONTENT_COLOR_1: String = "ED9E72"
    let CONTENT_COLOR_2: String = "C29CF1"
    static let screenSize: CGRect = UIScreen.main.bounds
    static let SCREEN_WIDTH: CGFloat = Constants.screenSize.width
    static let SCREEN_HEIGHT: CGFloat = Constants.screenSize.height

}
