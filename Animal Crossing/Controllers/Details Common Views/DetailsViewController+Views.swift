//
//  DetailsViewController+Views.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/26/20.
//

import Foundation
import UIKit
import ReactiveSwift

extension DetailsViewController {
    
    // - reusable to all detail types
    // - prepare according to its type
    func setupData() {
        displayLoadingScreen()
        switch detailType {
        case .bug, .fish, .fossil, .seaCreature:
            break
        case .villager:
            setupVillagerViewModelBinding()
            break
        case .none:
            break
        }
    }
    
    func setupVillagerDetailsView(villager: VillagerValue) {
        let villagerDetailsView = VillagerDetailsView(villager: villager)
        villagerDetailsView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(villagerDetailsView)
        villagerDetailsView.snp.makeConstraints { (make) in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom).offset(10)
            make.width.equalTo(Constants.SCREEN_WIDTH)
        }
        view.layoutIfNeeded()
    }
    
    func setVillagerHeaderData(villager: VillagerValue) {
        headerView.detailTitleLabel.text = villager.name
        headerView.detailImageView.image = ImageCache.publicCache.placeholderImage
        if let url = URL(string: villager.imageURL) {
            ImageCache.publicCache.load(url: url) { [weak self] (image) in
                DispatchQueue.main.async {
                    if let img = image {
                        self?.headerView.detailImageView.image = img
                    }
                }
            }
        }
    }
    
    func setupVillagerViewModelBinding() {
        let viewModel = VillagersViewModel()
        viewModel.fetchSingleVillager(queryItems: queryItems)
        viewModel.singleVillager.signal.observeValues { [weak self] villager in
            guard let self = `self` else {
                return
            }
            DispatchQueue.main.async {
                self.setupVillagerDetailsView(villager: viewModel.singleVillager.value)
                self.setVillagerHeaderData(villager: viewModel.singleVillager.value)
            }
        }
    }
    
}
