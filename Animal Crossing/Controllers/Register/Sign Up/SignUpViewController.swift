//
//  SignUpViewController.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/24/20.
//

import UIKit

class SignUpViewController: UIViewController {
    var delegate: LoginDelegate?
    var viewModel: UsersViewModel!
    var loginValue: LoginValue!
    
    var errorLabel = UILabel()
    var emailTextField = UITextField()
    var usernameTextField = UITextField()
    var passwordTextField = UITextField()
    var signUpButton =  UIButton()
    var loginButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = UsersViewModel(forSignUp: true)
        setupSignUpViewModelBinding()
    }
    
    override func loadView() {
        super.loadView()
        title = "Sign_Up".localized
        setupNavView()
        setBackgroud()
        setupButtons()
        setupTextFields()
        setupViews()
    }
    
    @objc func handleSwitchView() {
        delegate?.redirectToLogin()
    }
}

