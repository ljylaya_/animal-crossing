//
//  SignUpViewValidation.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/24/20.
//

import Foundation
import UIKit
import ReactiveSwift
import ReactiveCocoa

extension SignUpViewController {
    func isValidForm() -> Bool {
        guard let emailText = emailTextField.text, !emailText.isEmpty else { return false }
        guard let passwordText = passwordTextField.text, !passwordText.isEmpty else { return false }
        guard let usernameText = usernameTextField.text, !usernameText.isEmpty else { return false }
        return true
    }
    
    func setupViews() {
        let container = UIView(frame: .zero)
        let stackView = UIStackView(arrangedSubviews: [errorLabel, emailTextField, usernameTextField, passwordTextField, signUpButton, container])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(40)
            make.height.equalTo(230)
            make.width.equalTo(300)
            make.centerX.equalToSuperview()
        }
        
        container.addSubview(loginButton)
        loginButton.snp.makeConstraints { (make) in
            make.top.bottom.leading.trailing.equalToSuperview().inset(10)
        }
    }
    
    @objc func handleTextChange() {
        
        let emailText = emailTextField.text!
        let usernameText = usernameTextField.text!
        let passwordText = passwordTextField.text!
        
        let isFormFilled = !emailText.isEmpty && !usernameText.isEmpty && !passwordText.isEmpty
        
        if isFormFilled {
            signUpButton.backgroundColor = UIColor(hexString: ColorHex.hex3.rawValue)
            signUpButton.isEnabled = true
        }else {
            signUpButton.backgroundColor = UIColor.lightGray
            signUpButton.isEnabled = false
        }
    }
    
    func setupSignUpViewModelBinding() {
        signUpButton.reactive.controlEvents(.touchUpInside).observeValues { [weak self] _ in
            guard let self = `self` else { return }
            if !self.isValidForm() {
                return
            }
            
            guard let username = self.usernameTextField.text, let password = self.passwordTextField.text, let email = self.emailTextField.text else {
                return
            }
            let value = SignUpValue(username: username, email: email, password: password)
            self.viewModel.doSignUp(value: value)
        }
        
        viewModel.loginSignUpErrMessage.signal.observeValues { [weak self] (error) in
            guard let self = `self` else { return }
            if !error.isEmpty {
                self.errorLabel.isHidden = false
                self.errorLabel.text = error
            } else {
                self.delegate?.susscessSignUpLogin()
            }
        }
        
    }
}
