//
//  SignUpViews.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/25/20.
//

import Foundation
import UIKit

extension SignUpViewController {
    func setupTextFields(){
        setupErrorLabel()
        setupEmailField()
        setupUsernameField()
        setupPasswordField()
    }
    
    func setupButtons() {
        setupSignUpButton()
        setupLoginButton()
    }
    
    func setupErrorLabel() {
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        errorLabel.text = ""
        errorLabel.textColor = UIColor.red
        errorLabel.font = ACUtils.defaultAppFont(fontSize: 14)
        errorLabel.isHidden = true
    }
    
    func setupEmailField() {
        emailTextField.translatesAutoresizingMaskIntoConstraints = false
        emailTextField.placeholder = "Email".localized
        emailTextField.borderStyle = .roundedRect
        emailTextField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        emailTextField.backgroundColor = UIColor.white.withAlphaComponent(0.1)
    }
    
    func setupUsernameField() {
        usernameTextField.translatesAutoresizingMaskIntoConstraints = false
        usernameTextField.placeholder = "Username".localized
        usernameTextField.borderStyle = .roundedRect
        usernameTextField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        usernameTextField.backgroundColor = UIColor.white.withAlphaComponent(0.1)
    }
    
    func setupPasswordField() {
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextField.placeholder = "Password".localized
        passwordTextField.isSecureTextEntry = true
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        passwordTextField.backgroundColor = UIColor.white.withAlphaComponent(0.1)
    }
    
    func setupSignUpButton() {
        signUpButton.setTitle("Sign_Up".localized, for: .normal)
        signUpButton.setTitleColor(.white, for: .normal)
        signUpButton.titleLabel?.font = ACUtils.defaultAppFont(fontSize: 14)
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        signUpButton.layer.cornerRadius = 3
        signUpButton.backgroundColor = UIColor.lightGray
    }
    
    func setupLoginButton() {
        loginButton.setTitle("Login".localized, for: .normal)
        loginButton.setTitleColor(.lightGray, for: .normal)
        loginButton.titleLabel?.font = ACUtils.defaultAppFont(fontSize: 12)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.layer.cornerRadius = 3
    }
}
