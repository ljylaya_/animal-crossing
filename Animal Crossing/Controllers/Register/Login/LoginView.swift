//
//  LoginView.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/25/20.
//

import Foundation
import UIKit

extension LoginViewController {
    func setupTextFields(){
        setupErrorLabel()
        setupUsernameField()
        setupPasswordField()
    }
    
    func setupButtons() {
        setupSignUpButton()
        setupLoginButton()
    }
    
    func setupErrorLabel() {
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        errorLabel.text = ""
        errorLabel.textColor = UIColor.red
        errorLabel.font = ACUtils.defaultAppFont(fontSize: 14)
        errorLabel.isHidden = true
    }
    
    func setupUsernameField() {
        usernameTextField.translatesAutoresizingMaskIntoConstraints = false
        usernameTextField.placeholder = "Username".localized
        usernameTextField.borderStyle = .roundedRect
        usernameTextField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        usernameTextField.backgroundColor = UIColor.white.withAlphaComponent(0.1)
    }
    
    func setupPasswordField() {
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextField.placeholder = "Password".localized
        passwordTextField.isSecureTextEntry = true
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        passwordTextField.backgroundColor = UIColor.white.withAlphaComponent(0.1)
    }
    
    func setupSignUpButton() {
        signUpButton.setTitle("Sign_Up".localized, for: .normal)
        signUpButton.setTitleColor(.lightGray, for: .normal)
        signUpButton.titleLabel?.font = ACUtils.defaultAppFont(fontSize: 12)
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        signUpButton.layer.cornerRadius = 3
    }
    
    func setupLoginButton() {
        loginButton.setTitle("Login".localized, for: .normal)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.titleLabel?.font = ACUtils.defaultAppFont(fontSize: 14)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.layer.cornerRadius = 3
        loginButton.backgroundColor = UIColor.lightGray
    }
    
}
