//
//  LoginViewController.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/19/20.
//

import UIKit
import SnapKit

class LoginViewController: UIViewController {
    var delegate: LoginDelegate?
    var viewModel: UsersViewModel!
    var errorLabel = UILabel()
    var emailTextField = UITextField()
    var usernameTextField = UITextField()
    var passwordTextField = UITextField()
    var signUpButton =  UIButton()
    var loginButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = UsersViewModel(forSignUp: false)
        setupSignUpViewModelBinding()
    }
    
    override func loadView() {
        super.loadView()
        title = "Login".localized
        setupNavView()
        setBackgroud()
        setupTextFields()
        setupButtons()
        setupViews()
    }
    
    @objc func handleSwitchView() {
        delegate?.redirectToSignUp()
    }
    
    func setupViews() {
        let container = UIView(frame: .zero)
        let stackView = UIStackView(arrangedSubviews: [errorLabel, usernameTextField, passwordTextField, loginButton, container])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(40)
            make.height.equalTo(200)
            make.width.equalTo(300)
            make.centerX.equalToSuperview()
        }
        
        container.addSubview(signUpButton)
        signUpButton.snp.makeConstraints { (make) in
            make.top.bottom.leading.trailing.equalToSuperview().inset(10)
        }
        
    }
}
