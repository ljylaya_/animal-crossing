//
//  LoginViewValidation.swift
//  Animal Crossing
//
//  Created by Leah Joy Ylaya on 11/24/20.
//

import Foundation
import UIKit

extension LoginViewController {
    func isValidForm() -> Bool {
        guard let passwordText = passwordTextField.text, !passwordText.isEmpty else { return false }
        guard let usernameText = usernameTextField.text, !usernameText.isEmpty else { return false }
        return true
    }
    
    @objc func handleTextChange() {
        let usernameText = usernameTextField.text!
        let passwordText = passwordTextField.text!
        
        let isFormFilled = !usernameText.isEmpty && !passwordText.isEmpty
        
        if isFormFilled {
            signUpButton.backgroundColor = UIColor(hexString: ColorHex.hex3.rawValue)
            signUpButton.isEnabled = true
        }else {
            signUpButton.backgroundColor = UIColor.lightGray
            signUpButton.isEnabled = false
        }
    }
    
    func setupSignUpViewModelBinding() {
        signUpButton.reactive.controlEvents(.touchUpInside).observeValues { [weak self] _ in
            guard let self = `self` else { return }
            
            if !self.isValidForm () {
                return
            }
            
            guard let usernameEmail = self.usernameTextField.text, let password = self.passwordTextField.text else {
                return
            }
            let value = LoginValue(usernameOrEmail: usernameEmail, password: password)
            self.viewModel.doLogin(value: value)
            self.displayLoadingScreen()
        }
        
        viewModel.loginSignUpErrMessage.signal.observeValues { [weak self] (error) in
            guard let self = `self` else { return }
            if !error.isEmpty {
                self.errorLabel.isHidden = false
                self.errorLabel.text = error
            } else {
                self.delegate?.susscessSignUpLogin()
            }
        }
        
    }
}
